//
//  LifehacksTests.swift
//  LifehacksTests
//
//  Created by Marcos Rebouças on 02/12/19.
//  Copyright © 2019 Marcos Rebouças. All rights reserved.
//

import XCTest
@testable import Lifehacks

class QuestionTests: XCTestCase {
    func testVoteUp() {
        var question = makeQuestion()
        question.voteUp()
        XCTAssertEqual(question.score, 1)
    }
    
    func testVoteDown() {
        var question = makeQuestion()
        question.voteDown()
        XCTAssertEqual(question.score, -1)
    }
    
    func testUnvote() {
        var question = makeQuestion()
        question.voteUp()
        question.unvote()
        XCTAssertEqual(question.score, 0)
    }
    
    func testVotingOnce() {
        var question = makeQuestion()
        question.voteUp()
        question.voteUp()
        XCTAssertEqual(question.score, 1)
    }
    
    func testReversingVote() {
        var question = makeQuestion()
        question.voteUp()
        question.voteDown()
        XCTAssertEqual(question.score, -1)
    }
}

private extension QuestionTests {
    func makeQuestion() -> Question {
        let user = User(name: "", aboutMe: "", reputation: 0, avatar: UIImage())
        let firstComment = Comment(id: 0, body: "In the tumultuous business of cutting-in and attending to a whale, there is much running backwards and forwards among the crew.", owner: user)
        let seconfComment = Comment(id: 0, body: "In the tumultuous business of cutting-in and attending to a whale, there is much running backwards and forwards among the crew.", owner: user)
        let firstAnswer = Answer(id: 0, body: "So strongly and metaphysically did I conceive of my situation then, that while earnestly watching his motions", creationDate: Date(), owner: user, score: 120)
        let secondAnswer = Answer(id: 0, body: "So strongly and metaphysically did I conceive of my situation then, that while earnestly watching his motions", creationDate: Date(), owner: user, score: 120)
        return Question(id: 0, viewCount: 0, answerCount: 200, title: "", body: "", isAnswered: true, creationDate: Date(), tags: [], owner: user, comments: [firstComment, seconfComment], answers: [firstAnswer, secondAnswer], score: 0)
    }
}
