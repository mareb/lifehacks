//
//  ContentView.swift
//  Lifehacks
//
//  Created by Marcos Rebouças on 02/12/19.
//  Copyright © 2019 Marcos Rebouças. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        QuestionView(question: TestData.question)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
            .environment(\.sizeCategory, .accessibilityExtraExtraExtraLarge)
            .background(Color(.systemBackground))
            .environment(\.colorScheme, .dark)
            .previewDevice("iPhone SE")
    }
}
