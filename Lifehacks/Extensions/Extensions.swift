//
//  Extensions.swift
//  Lifehacks
//
//  Created by Marcos Rebouças on 02/12/19.
//  Copyright © 2019 Marcos Rebouças. All rights reserved.
//

import Foundation
import SwiftUI

extension Int {
    var formatted: String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        return formatter.string(from: NSNumber(value: self)) ?? ""
    }
}
 
extension Date {
    var formatted: String {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        return formatter.string(from: self)
    }
}

extension Font {
    static func uiFont(style: UIFont.TextStyle) -> Font {
        let font = UIFont.preferredFont(forTextStyle: style)
        return Font.system(size: font.pointSize, weight: .regular, design: .default)
    }
}

// MARK: Color and Gradient

extension Color {
    static var lightBlue: Color { Color(UIColor.systemBlue.colorWithOffsets(saturation: -0.5)) }
    static var lightOrange: Color { Color(UIColor.systemOrange.colorWithOffsets(saturation: -0.5)) }
    static var lightGreen: Color { Color(UIColor.systemGreen.colorWithOffsets(saturation: -0.15, brightness: 0.2)) }
    static var teal: Color { Color(UIColor.systemTeal) }
    static var lightTeal: Color { Color(UIColor.systemTeal.colorWithOffsets(saturation: -0.3)) }
}

extension LinearGradient {
    static var blue: Self { verticalGradient(with: [.lightBlue, .blue]) }
    static var orange: Self { verticalGradient(with: [.lightOrange, .orange]) }
    static var green: Self { verticalGradient(with: [.lightGreen, .green]) }
    static var teal: Self { verticalGradient(with: [.lightTeal, .teal]) }
 
    private static func verticalGradient(with colors: [Color]) -> Self {
        let gradient = Gradient(colors: colors)
        return LinearGradient(gradient: gradient, startPoint: UnitPoint(x: 0.0, y: 0.0), endPoint: UnitPoint(x: 0.0, y: 1.0))
    }
}

// This is what this UIColor extension below does:

// The hsba property extracts the hue, saturation, brightness, and alpha of a color. I use those instead of red, green, and blue (RGB) values because it’s easier to make a color lighter by affecting its saturation and brightness. Notice that this computed property does not return a single value, but four, using a tuple.
// The colorWithOffsets(hue:saturation:brightness:alpha:) method creates a new color from an existing one, offsetting its HSBA properties. These are offsets and not absolute values, so we can increase or decrease each property as we please. The parameters of these methods all have a default value since we often need to change only one property.

extension UIColor {
    var hsba: (hue: CGFloat, saturation: CGFloat, brightness: CGFloat, alpha: CGFloat) {
        var hue: CGFloat = 0.0
        var saturation: CGFloat = 0.0
        var brightness: CGFloat = 0.0
        var alpha: CGFloat = 0.0
        self.getHue(&hue, saturation: &saturation, brightness: &brightness, alpha: &alpha)
        return (hue, saturation, brightness, alpha)
    }
    
    func colorWithOffsets(hue: CGFloat = 0.0, saturation: CGFloat = 0.0, brightness: CGFloat = 0.0, alpha: CGFloat  = 0.0) -> UIColor {
        UIColor(
            hue: hsba.hue + hue,
            saturation: hsba.saturation + saturation,
            brightness: hsba.brightness + brightness,
            alpha: hsba.alpha + alpha
        )
    }
}

// MARK: Modifier

// Structures that conform to the ViewModifier are not views. So, to apply a view modifier to a view, we to either wrap it inside a ModifiedContent value, which is a view, or use the .modifier modifier, which does precisely that.

// That’s still a bit clunky and quite not like using other modifiers. Luckily, we can extend the View protocol and write some new methods to bridge that final gap:

extension View {
    func blueStyle() -> some View {
        modifier(Style(gradient: .blue))
    }
    
    func tealStyle() -> some View {
        modifier(Style(gradient: .teal))
    }
    
    func orangeStyle(filled: Bool = true) -> some View {
        modifier(Style(gradient: .orange, filled: filled))
    }
    
    func greenStyle(filled: Bool = true) -> some View {
        modifier(Style(gradient: .green, filled: filled))
    }
}

extension ContentSizeCategory {
    var isLarge: Bool {
        let largeCategories: [ContentSizeCategory] = [
            .accessibilityLarge,
            .accessibilityExtraLarge,
            .accessibilityExtraExtraLarge,
            .accessibilityExtraExtraExtraLarge
        ]
        return largeCategories.contains(self)
    }
}
