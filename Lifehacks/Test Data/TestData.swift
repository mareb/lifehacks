//
//  TestData.swift
//  Lifehacks
//
//  Created by Marcos Rebouças on 02/12/19.
//  Copyright © 2019 Marcos Rebouças. All rights reserved.
//

import UIKit

struct TestData {
    static let shortText = "In the tumultuous business of cutting-in and attending to a whale, there is much running backwards and forwards among the crew."
    static let longText = "So strongly and metaphysically did I conceive of my situation then, that while earnestly watching his motions, I seemed distinctly to perceive that my own individuality was now merged in a joint stock company of two; that my free will had received a mortal wound; and that another's mistake or misfortune might plunge innocent me into unmerited disaster and death."
    static let tags = ["monkey", "rope", "found", "all", "whalers"]
    static let user = User(name: "Betty Vasquez", aboutMe: longText, reputation: 1234, avatar: #imageLiteral(resourceName: "avatar"))
    static let otherUser = User(name: "Martin Abasto", aboutMe: longText, reputation: 986, avatar: #imageLiteral(resourceName: "avatar"))
    static let answer = Answer(id: 0, body: longText, creationDate: Date(), owner: otherUser, score: 124)
    static let comment = Comment(id: 0, body: longText, owner: otherUser)
    
    static var question = Question(
        id: 0,
        viewCount: 2770,
        answerCount: 3,
        title: shortText,
        body: longText,
        isAnswered: true,
        creationDate: Date(),
        tags: tags,
        owner: user,
        comments: [comment, comment ,comment],
        answers: [answer, answer, answer],
        score: 359
    )
        
    static let questions = [ question, question, question ]
}
