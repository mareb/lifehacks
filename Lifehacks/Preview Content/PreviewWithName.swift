//
//  PreviewWithName.swift
//  Lifehacks
//
//  Created by Marcos Rebouças on 16/01/20.
//  Copyright © 2020 Marcos Rebouças. All rights reserved.
//

import SwiftUI

extension View {
    func previewWithName(_ name: String) -> some View {
        self
            .padding()
            .previewLayout(.sizeThatFits)
            .previewDisplayName(name)
    }
    
    func allPreviews() -> some View {
        Group {
            self
            self
                .background(Color(.systemBackground))
                .environment(\.colorScheme, .dark)
                .previewDisplayName("Dark mode")
            self
                .environment(\.sizeCategory, .accessibilityExtraExtraExtraLarge)
                .previewDisplayName("Accessibility XXXL")
            self
                .previewDevice("iPhone SE")
                .previewDisplayName("iPhone SE")
        }
    }
}

extension String {
    static func name<T>(for type: T) -> String {
        String(reflecting: type)
            .components(separatedBy: ".")
            .last ?? ""
    }
}
