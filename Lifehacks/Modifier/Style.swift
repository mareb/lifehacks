//
//  Style.swift
//  Lifehacks
//
//  Created by Marcos Rebouças on 04/12/19.
//  Copyright © 2019 Marcos Rebouças. All rights reserved.
//

import SwiftUI

struct Style: ViewModifier {
    let gradient: LinearGradient
    var filled = true
    
    func body(content: Content) -> some View {
        Group {
        if filled {
            content
                .background(gradient)
                .cornerRadius(6.0)
                .foregroundColor(.white)
        } else {
            content
            .background(
                RoundedRectangle(cornerRadius: 6.0)
                    .strokeBorder(gradient, lineWidth: 2.0))
            }
        }
    }
}

// Opaque types require type returned by a function to always be the same. So it won't compile if we just use a if else statement because it'll be returning different types.
// The preferred alternative is to use a Group. Keep in mind that using a Group will not always work. Complex functions cannot be written declaratively. In those cases, you need to use AnyView.
// To return AnyView for both return statements, do it like: AnyView(content .background(gradient).cornerRadius(6.0).foregroundColor(.white)


// Content definition: func body(content: Content) -> some View
// So anyway, we can see that Content is an alias for _ViewModifier_Content<Self>, which is a struct that doesn't define any interesting public interface, but does (in an extension) conform to View. So this tells us that, when we write our own ViewModifier, our body method will receive some sort of View (the specific type is defined by the framework and we can just call it Content), and return some sort of View (we get to pick the specific return type).
