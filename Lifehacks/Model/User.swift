//
//  User.swift
//  Lifehacks
//
//  Created by Marcos Rebouças on 02/12/19.
//  Copyright © 2019 Marcos Rebouças. All rights reserved.
//

import UIKit

struct User {
    var name: String
    var aboutMe: String
    var reputation: Int
    var avatar: UIImage
}
