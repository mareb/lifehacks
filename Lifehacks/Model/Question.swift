//
//  Question.swift
//  Lifehacks
//
//  Created by Marcos Rebouças on 02/12/19.
//  Copyright © 2019 Marcos Rebouças. All rights reserved.
//

import Foundation

struct Question: Identifiable, Votable {
    let id: Int
    let viewCount: Int
    let answerCount: Int
    let title: String
    let body: String
    let isAnswered: Bool
    let creationDate: Date
    let tags: [String]
    let owner: User
    let comments: [Comment]
    var answers: [Answer]
    var score: Int
    var vote = Vote.none
}

struct Answer: Identifiable, Votable {
    let id: Int
    let body: String
    let creationDate: Date
    let owner: User
    var score: Int
    var vote = Vote.none
}
 
struct Comment: Identifiable {
    let id: Int
    let body: String
    let owner: User
}

// MARK: Vote
protocol Votable {
    var vote: Vote { get set }
    var score: Int { get set }
}

enum Vote: Int {
    case none = 0
    case up = 1
    case down = -1
}

extension Votable {
    mutating func voteUp() {
        cast(vote: .up)
    }

    mutating func voteDown() {
        cast(vote: .down)
    }
    
    mutating func unvote() {
        score -= vote.rawValue
        vote = .none
    }
}

private extension Votable {
    mutating func cast(vote: Vote) {
        guard self.vote != vote else { return }
        unvote()
        score += vote.rawValue
        self.vote = vote
    }
}

// Notice that we treat protocols like any other type. The cast(vote:) method is still only meant for internal use, like it was in the Question type, so it still resides in a private extension.
