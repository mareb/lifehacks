//
//  SettingsView.swift
//  Lifehacks
//
//  Created by Marcos Rebouças on 20/01/20.
//  Copyright © 2020 Marcos Rebouças. All rights reserved.
//

import SwiftUI

struct SettingsView: View {
    @State var selectedTheme: Theme = .default
    
    var body: some View {
        Form {
            Section(header: Text("APP THEME")) {
                ForEach(Theme.allThemes) { theme in
                    Row(name: theme.name, selected: theme.id == self.selectedTheme.id) {
                        self.selectedTheme = theme
                    }
                }
            }
        }
    }
}
 
extension SettingsView {
    struct Row: View {
        let name: String
        let selected: Bool
        let tags = ["tag1", "tag2", "tag3", "tag4", "tag5"]
        var action: () -> Void
        
        var body: some View {
            Button(action: action) {
                HStack {
                    TopQuestionsView.Row(title: name, tags: tags, score: 999, answerCount: 99, viewCount: 999999, date: Date(), name: "Username", isAnswered: true)
                    Spacer()
                    Image(systemName: "checkmark")
                        .font(.headline)
                        .foregroundColor(selected ? .accentColor : .white)
                }
            }
        }
    }
}

struct SettingsView_Previews: PreviewProvider {
    typealias Row = SettingsView.Row
    
    static var previews: some View {
        Group {
            SettingsView()
        VStack {
            Row(name: "Name", selected: false, action: {})
            Row(name: "Name", selected: true, action: {})
        }
        .previewWithName(String.name(for: Row.self))
        }
    }
}
