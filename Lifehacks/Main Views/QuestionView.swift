//
//  ContentView.swift
//  Lifehacks
//
//  Created by Marcos Rebouças on 02/12/19.
//  Copyright © 2019 Marcos Rebouças. All rights reserved.
//

import SwiftUI

struct QuestionView: View {
    @State var question: Question
    
    var body: some View {
        List {
            QuestionDetails(question: $question)
            Comments(comments: question.comments)
                .listRowInsets(.init(top: 24, leading: 0, bottom: 24, trailing: 0))
            ForEach(question.answers.indices) { index in
                AnswerDetails(answer: self.$question.answers[index])
                    .padding(.vertical, 24.0)
            }
        }
        .buttonStyle(PlainButtonStyle())
    }
}

// MARK: - QuestionView Subviews

extension QuestionView {
    struct Info: View {
        let title: String
        let viewCount: Int
        let date: Date
        let tags: [String]
        
        var body: some View {
            VStack(alignment: .leading, spacing: 8.0) {
                Text(title)
                    .font(.headline)
                TagsView(tags: tags)
                Group {
                    Text("Asked on \(date.formatted)")
                    Text("Viewed \(viewCount.formatted) times")
                }
                .font(.caption)
                .foregroundColor(.secondary)
            }
        }
    }
}

extension QuestionView {
    struct Owner: View {
        let name: String
        let reputation: Int
        let avatar: UIImage
        
        var body: some View {
            HStack {
                RoundImage(image: avatar)
                    .frame(width: 48, height: 48)
                VStack(alignment: .leading, spacing: 4.0) {
                    Text(name)
                        .font(.headline)
                    Text("\(reputation.formatted) reputation")
                        .font(.caption)
                }
            }
            .padding(16)
        }
    }
}

extension QuestionView.Owner {
    init(user: User) {
        name = user.name
        reputation = user.reputation
        avatar = user.avatar
    }
}

// MARK: - QuestionView Previews

struct QuestionView_Previews: PreviewProvider {
    typealias Owner = QuestionView.Owner
    typealias Comments = QuestionView.Comments
    typealias SingleComment = Comments.SingleComment
    
    static let question = TestData.question
    static let user = TestData.user
    static let comment = TestData.comment
    
    static var previews: some View {
        Group {
            QuestionView(question: question)
                .allPreviews()
            Owner(user: user)
            .blueStyle()
            .previewWithName(String.name(for: Owner.self))
            Comments(comments: question.comments)
                .previewWithName(String.name(for: Comments.self))
            SingleComment(text: comment.body, ownerName: comment.owner.name)
                .previewWithName(String.name(for: SingleComment.self))
        }
    }
}

struct Styles_Previews: PreviewProvider {
    
    static var stack: some View {
        VStack(spacing: 16) {
            Text("Blue Style")
            .padding()
            .blueStyle()
            Text("Orange Filled Style")
            .padding()
            .orangeStyle()
            Text("Orange Empty Style")
            .padding()
            .orangeStyle(filled: false)
            Text("Teal Style")
            .padding()
            .tealStyle()
            Text("Green Style")
            .padding()
            .greenStyle()
            Text("Green Empty Style")
            .padding()
            .greenStyle(filled: false)
            
            }
        }
    
    static var previews: some View {
        Group {
            stack
                .padding()
                .previewDisplayName("Light Mode")
            
            stack
                .padding()
                .background(Color(.systemBackground))
                .environment(\.colorScheme, .dark)
                .previewDisplayName("Dark Mode")
        }
        .previewLayout(.sizeThatFits)
    }
}
