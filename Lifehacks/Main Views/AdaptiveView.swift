//
//  AdaptiveView.swift
//  Lifehacks
//
//  Created by Marcos Rebouças on 13/03/20.
//  Copyright © 2020 Marcos Rebouças. All rights reserved.
//

import SwiftUI

struct AdaptiveView<Standard: View, Large: View>: View {
    let standard: Standard
    let large: Large
    
    @Environment (\.sizeCategory) private var sizeCategory
    
    var body: some View {
        Group {
            if sizeCategory.isLarge {
                large
            } else {
                standard
            }
        }
    }
}
