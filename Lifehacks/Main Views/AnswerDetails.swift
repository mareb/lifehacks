//
//  AnswerDetails.swift
//  Lifehacks
//
//  Created by Marcos Rebouças on 22/01/20.
//  Copyright © 2020 Marcos Rebouças. All rights reserved.
//

import SwiftUI

extension QuestionView {
    struct AnswerDetails: View {
        @Binding var answer: Answer
        
        var body: some View {
            HStack(alignment: .top, spacing: 16.0) {
                QuestionView.Voting(score: answer.score, vote: .init(vote: answer.vote), upvote: { self.answer.voteUp() }, downvote: { self.answer.voteDown() }, unvote: { self.answer.unvote() })
                VStack(alignment: .leading, spacing: 8.0) {
                    Text(answer.body)
                        .font(.subheadline)
                    Text("Answered on \(answer.creationDate.formatted)")
                        .font(.caption)
                        .foregroundColor(.secondary)
                    HStack {
                        Spacer()
                        QuestionView.Owner(user: answer.owner)
                            .orangeStyle()
                    }
                    .padding(.top, 16.0)
                }
            }
        }
    }
}
 
struct QuestionView_Answer_Previews: PreviewProvider {
    typealias Details = QuestionView.AnswerDetails
    
    static let answer = TestData.answer
    
    static var previews: some View {
        Details(answer: .constant(answer))
            .previewWithName(.name(for: Details.self))
    }
}
