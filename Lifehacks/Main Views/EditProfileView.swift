//
//  EditProfileView.swift
//  Lifehacks
//
//  Created by Marcos Rebouças on 16/01/20.
//  Copyright © 2020 Marcos Rebouças. All rights reserved.
//

import SwiftUI

struct EditProfileView: View {
    @State var user: User
    
    var body: some View {
        VStack {
            Header(name: $user.name, avatar: user.avatar)
            AboutMe(text: $user.aboutMe)
            Spacer()
        }
        .padding(20.0)
        .animation(.default)
    }
}

extension EditProfileView {
    struct ErrorMessage: View {
        let text: String
        var isVisible: Bool = false
        
        var body: some View {
            Group {
                if isVisible {
                    Text(text)
                        .font(.footnote)
                        .bold()
                        .foregroundColor(.orange)
                }
            }
        }
    }
}

extension EditProfileView {
    struct AboutMe: View {
        @Binding var text: String
        
        var body: some View {
            VStack(alignment: .leading) {
                Text("About me:")
                    .font(.callout)
                    .bold()
                TextField("Write something about yourself", text: $text)
                EditProfileView.ErrorMessage(text: "The about me cannot be empty", isVisible: text.isEmpty)
            }
        }
    }
}

extension EditProfileView {
    struct Header: View {
        @Binding var name: String
        var avatar: UIImage
        
        var body: some View {
            HStack(alignment: .top) {
                RoundImage(image: avatar, borderColor: .blue)
                    .frame(width: 62.0, height: 62.0)
                VStack(alignment: .leading) {
                    TextField("Name:", text: $name)
                    Divider()
                    EditProfileView.ErrorMessage(text: "The name cannot be empty", isVisible: name.isEmpty)
                }
                .padding(.leading, 16)
            }
        }
    }
}

struct EditProfileView_Previews: PreviewProvider {
    typealias Header = EditProfileView.Header
    typealias AboutMe = EditProfileView.AboutMe
    typealias ErrorMessage = EditProfileView.ErrorMessage
    
    static let user = TestData.user
    
    static var previews: some View {
        Group {
            EditProfileView(user: user)
            
                VStack(spacing: 16.0) {
                    Header(name: .constant(user.name), avatar: user.avatar)
                    Header(name: .constant(""), avatar: user.avatar)
                }
                .previewWithName(String.name(for: Header.self))
                
                VStack(spacing: 16.0) {
                    AboutMe(text: .constant(user.aboutMe))
                    AboutMe(text: .constant(""))
                }
                .previewWithName(String.name(for: AboutMe.self))
                
                ErrorMessage(text: "The name cannot be empty", isVisible: true)
                .previewWithName(String.name(for: ErrorMessage.self))
        }
    }
}
