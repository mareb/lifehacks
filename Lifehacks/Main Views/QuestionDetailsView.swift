//
//  QuestionDetailsView.swift
//  Lifehacks
//
//  Created by Marcos Rebouças on 22/01/20.
//  Copyright © 2020 Marcos Rebouças. All rights reserved.
//

import SwiftUI

extension QuestionView {
    struct QuestionDetails: View {
        @Binding var question: Question
        
        var body: some View {
            VStack(alignment: .leading, spacing: 24.0) {
                VStack(alignment: .leading, spacing: 24.0) {
                    AdaptiveView(
                        standard: HStack(alignment: .top, spacing: 16.0) { topContent },
                        large: VStack { topContent })
                }
                Text(question.body)
                    .font(.subheadline)
                HStack {
                    Spacer()
                    QuestionView.Owner(user: question.owner)
                    .blueStyle()
                }
            }
        }
    }
}

private extension QuestionView.QuestionDetails {
    var topContent: some View {
        Group {
            QuestionView.Voting(
                score: question.score,
                vote: .init(vote: question.vote),
                upvote: { self.question.voteUp() },
                downvote: { self.question.voteDown() },
                unvote: { self.question.unvote() }
            )
            Info(title: question.title, viewCount: question.viewCount, date: question.creationDate, tags: question.tags)
        }
    }
}

extension QuestionView.QuestionDetails {
    struct Info: View {
        let title: String
        let viewCount: Int
        let date: Date
        let tags: [String]
        
        var body: some View {
            VStack(alignment: .leading, spacing: 8.0) {
                Text(title)
                    .font(.headline)
                TagsView(tags: tags)
                Group {
                    Text("Asked on \(date.formatted)")
                    Text("Viewed \(viewCount.formatted) times")
                }
                .font(.caption)
                .foregroundColor(.secondary)
            }
        }
    }
}

// MARK: Previews

struct QuestionView_Details_Previews: PreviewProvider {
    typealias QuestionDetails = QuestionView.QuestionDetails
    typealias Info = QuestionDetails.Info
    
    static let question = TestData.question
 
    static var previews: some View {
        Group {
            QuestionDetails(question: .constant(question))
                .previewWithName(String.name(for: QuestionDetails.self))
            QuestionDetails(question: .constant(question))
                .environment(\.sizeCategory, .accessibilityExtraExtraExtraLarge)
                .previewWithName(String.name(for: QuestionDetails.self) + " XXXL")
            Info(title: question.title, viewCount: question.viewCount, date: question.creationDate, tags: question.tags)
                .previewWithName(String.name(for: Info.self))
        }
    }
}
