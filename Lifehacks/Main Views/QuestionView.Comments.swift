//
//  QuestionView.Comments.swift
//  Lifehacks
//
//  Created by Marcos Rebouças on 23/01/20.
//  Copyright © 2020 Marcos Rebouças. All rights reserved.
//

import SwiftUI

extension QuestionView {
    struct Comments: View {
        let comments: [Comment]
        
        var body: some View {
            GeometryReader { geometry in
            ScrollView(.horizontal, showsIndicators: false) {
                HStack(alignment: .top) {
                    ForEach(self.comments) { comment in
                        SingleComment(text: comment.body, ownerName: comment.owner.name)
                            .frame(width: geometry.size.width - 32)
                    }
                }
                .padding(.horizontal)
             }
          }
          .frame(height: 174.0)
        }
    }
}

extension QuestionView.Comments {
    struct SingleComment: View {
        let text: String
        let ownerName: String
        
        var body: some View {
            VStack(alignment: .leading, spacing: 8.0) {
                Text(text)
                .lineLimit(5)
                Button(action: {}) {
                    Text(ownerName)
                        .foregroundColor(.accentColor)
                }
            }
            .font(.subheadline)
            .padding(24)
            .background(Color(UIColor.systemGray6))
            .cornerRadius(6)
        }
    }
}
