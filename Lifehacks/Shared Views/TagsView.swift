//
//  TagsView.swift
//  Lifehacks
//
//  Created by Marcos Rebouças on 03/12/19.
//  Copyright © 2019 Marcos Rebouças. All rights reserved.
//

import SwiftUI

struct TagsView: View {
    let tags: [String]
    
    var body: some View {
        Text(tagsString)
            .font(.footnote)
            .fontWeight(.semibold)
            .foregroundColor(.blue)
    }
}

private extension TagsView {
    var tagsString: String {
        var result = tags.first ?? ""
        for tag in tags.dropFirst() {
            result.append(", " + tag)
        }
        return result
    }
}

struct TagsView_Previews: PreviewProvider {
    static var previews: some View {
        TagsView(tags: ["lorem", "ipsum", "dolor", "sit", "amet"])
            .previewLayout(.sizeThatFits)
    }
}
